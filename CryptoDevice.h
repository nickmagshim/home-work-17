#pragma once

#include <cstdio>
#include <iostream>
#include <string.h>
#include <cstdlib>
#include "../cryptopp700/osrng.h"
#include "../cryptopp700/modes.h"

class CryptoDevice
{

public:
    std::string encryptAES(std::string);
    std::string decryptAES(std::string);


private:
    CryptoPP::byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];

};
